# My project's README

The script should do the following:
- align all references. (all sub-b. all sub-c. ... difference reference sets.)
- use "maff --addfragment" to align your primer to the ref.
- cut out the section that it aligns to
- remove the primer seq from this cut out file.
- calc base frequencies from this cut out file.
- produce a logogram.
- produce stats about the cut out section. (%A, %C, etc.) (.csv)
- generate cons. seq.