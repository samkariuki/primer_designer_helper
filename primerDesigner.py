#!/usr/bin/env python
"""
Designing primers shouldn't be difficult. Lets make it easier.
"""
# Author: David Matten (david.matten@uct.ac.za)

import weblogolib
import argparse
import subprocess
import sys, os

def main(panelFileName, primer_file, output_location):
# Step 1
# align into panel.
    our_output_file = output_location + "/output.fasta"
    out_mafft_call = "mafft --op 50 --addfragments %s %s > %s" %(primer_file, panelFileName, our_output_file)
    subprocess.call(out_mafft_call, shell=True)


# step 2
# cut to primer region
# get primer seqid... we have the name of the file.
    from smallBixTools import smallBixTools as st
# take fasta file - get it into python so we can work with it here....

    primer_dictionary = st.fasta_to_dct(primer_file)
    print(primer_dictionary)
    primer_name = primer_dictionary.keys() # doing this to get the string of the key....
    print(primer_name) # this is still a dictionray_keys object.
    list_primer_name = list(primer_name) #convert that objcet ot a list.
    print(list_primer_name)

    # what happens if the input file has more than one sequence?
    if len(list_primer_name) > 1:
        print("more than one primer sequence found... you idiot")
        sys.exit()

    first_primer_name = list_primer_name[0]
    print(first_primer_name)

    # find value in alignment.
# turn the alignment into a dictionary in python
    alignment_dictionary = st.fasta_to_dct(our_output_file)
    #print(alignment_dictionary) # proving to ourselves that the variable contains what we think it should

    # grab the aligned sequence for the primer into a new variable.
    aligned_primer_seq = alignment_dictionary[first_primer_name]
    print(aligned_primer_seq)


    # SUPER IMPORTANT TO KEEP FOCUS!!! WHAT ARE WE TRYING TO GET HERE?!?!?!
# start position.
# ONLY the start position.

    count = 0
    for i in aligned_primer_seq:
        if i == "-":
            count += 1
        if i != "-":
            break
    print(count)
    start_position = count +1
    end_position = start_position + len(primer_dictionary[first_primer_name])
    print("primer start position: %s" %start_position)
    print("primer end position: %s" %end_position)

    # find how many gaps are in the alignment before the primer


# THIS week.....
# cut the pieces out of all the sequences.
# and write them to a .fasta file.
# AND MORE!!!! Lets sort the sequences, so our primer is at the top.

                        #uct/dev/source/primer_designing/primer_alignment.fasta
    cut_filename = os.path.join(output_location, "primer_alignment.fasta")

    file_writer = open(cut_filename, "w")
    # lets write the primer here.
    file_writer.write(">" + first_primer_name + "\n")
    file_writer.write(primer_dictionary[first_primer_name] + "\n")


    for seqids, sequences in alignment_dictionary.items():
        # lets check if its the primer here, and if it is: dont write it.

        # lets cut the piece we want from the seuqnece.
        print(seqids)
        print(sequences[start_position-1:end_position-1])
        cut_seq_or_something = sequences[start_position-1:end_position-1]

        if (seqids != first_primer_name): #seqid is equal to primer name
            # do something
            file_writer.write(">" + seqids + "\n")
            file_writer.write(cut_seq_or_something + "\n")

    file_writer.close()

    our_logo_file = output_location + "/logo.png"
    out_weblogo_call = "weblogo -U 'probability'  < %s > %s" %(cut_filename, our_logo_file)
    subprocess.call(out_weblogo_call, shell=True)


    # STEP NEXT?!
    # a few base pairs before and after
    # find complementary.
    # GC content of the primer.
    # write a .csv file of exact percentages, to compliment the weblogo.



if __name__ == "__main__":
    parser = argparse.ArgumentParser(description='our snazzy primer designer.')
    parser.add_argument('-panel', '--panel_file', type=str, required=True,
                        help='Please give panel_file. Pretty please.')
    parser.add_argument('-primer', '--primer_file', type=str, required=True,
                        help='Please give primer_file. Pretty please.')
    parser.add_argument('-output', '--output_location', type=str, required=True,
                        help='please give output location. Pretty please.')

    args = parser.parse_args()

    panelFileName = args.panel_file
    primer_file = args.primer_file
    output_location = args.output_location

    main(panelFileName, primer_file, output_location)